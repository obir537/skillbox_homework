﻿#include <iostream>

using namespace std;

class Animal
{
public:
    virtual void Voice()
    {
        cout << "Animal" << endl;
    }
};

class Cat : public Animal
{
public:
    void Voice() override
    {
        cout << "Cat" << endl;
    }
};

class Woolf : public Animal
{
public:
    void Voice() override
    {
        cout << "Woolf " << endl;
    }
};

class Dog : public Woolf
{
public:
    void Voice() override
    {
        cout << "Dog" << endl;
    }
};



int main()
{
    setlocale(LC_ALL, "Russian");

    Animal* A = new Animal;
    Cat* C = new Cat;
    Woolf* W = new Woolf;
    Dog* D = new Dog;

    Animal* Animals[4]{A, C, W, D};
    for (int i = 0; i < 4; i++)
    {
        Animals[i]->Voice();
    }
   

    return 0;
}